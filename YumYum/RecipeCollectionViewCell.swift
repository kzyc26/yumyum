//
//  RecipeCollectionViewCell.swift
//  YumYum
//
//  Created by Kezia Yovita Chandra on 19/08/21.
//

import UIKit
import SDWebImage

class RecipeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeName: UILabel!
    @IBOutlet weak var recipeTime: UILabel!
    
    func updateView(recipe: Recipes,index:Int){
        recipeName.text = recipe.name
        recipeTime.text = "\(convertToMinute(sec: recipe.timers)) Minutes"
        recipeImage.sd_setImage(with: URL(string: recipe.imageURL), completed: nil)
        configureView(index: index)
    }
    func configureView(index:Int){
        if index.isMultiple(of: 2){
            background.backgroundColor = UIColor(named: "Grey")
            recipeName.textColor = UIColor(named: "Theme2")
        }
        else{
            background.backgroundColor = UIColor(named: "Theme1")
            recipeName.textColor = UIColor.lightGray
            
        }
        recipeImage.layer.cornerRadius = 10
        recipeImage.layer.maskedCorners=[.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    func convertToMinute(sec:[Int]) -> String{
        print("kepanggil")
        var mins = sec.reduce(0,+)
        mins = mins/60
        if mins == 0 {
            return "15"
        }
        return String(mins)
    }
    
}
