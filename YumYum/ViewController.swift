//
//  ViewController.swift
//  YumYum
//
//  Created by Kezia Yovita Chandra on 18/08/21.
//

import UIKit
import Foundation
import WatchConnectivity

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var recipeCollectionView: UICollectionView!
    @IBOutlet weak var searchBackground : UIView!
    var recipes = DataLoader.init().allRecipes
    var kirimResep:Recipes?
    var watchSession:WCSession?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureWatchKitSession()
        delegation()
    }

    func delegation(){
        recipeCollectionView.delegate = self
        recipeCollectionView.dataSource = self
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = recipeCollectionView.dequeueReusableCell(withReuseIdentifier: "RecipeChoice", for: indexPath) as? RecipeCollectionViewCell
        cell!.updateView(recipe: recipes[indexPath.row], index: indexPath.row)
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        kirimResep =  recipes[indexPath.row]
        movetoDetail(dataRecipe: kirimResep!)
    }
    func movetoDetail(dataRecipe: Recipes){
        let detailVC = storyboard?.instantiateViewController(identifier: "DetailPage") as! DetailPageViewController
        detailVC.recipe = dataRecipe
        detailVC.watchSession = watchSession
        self.navigationController?.pushViewController(detailVC, animated: false)
//        self.present(detailVC, animated: true, completion: nil)
    }
    
 
}
extension ViewController: WCSessionDelegate{
    func configureWatchKitSession(){
        if WCSession.isSupported(){
            watchSession = WCSession.default
            watchSession?.delegate = self
            watchSession?.activate()
        }
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        debugPrint(error as Any)
        print("activationDidComplete", activationState)
        print("WCSession.default activated on iPhone")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("sessionbecomeinactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("session deactivate")
    }
 
}

