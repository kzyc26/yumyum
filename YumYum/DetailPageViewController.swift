//
//  DetailPageViewController.swift
//  YumYum
//
//  Created by Kezia Yovita Chandra on 19/08/21.
//
import UIKit
import SDWebImage
import WatchConnectivity

class DetailPageViewController: UIViewController {
    @IBOutlet weak var imageRecipe: UIImageView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var recipeName: UILabel!
    @IBOutlet weak var recipeTimer: UILabel!
    @IBOutlet weak var recipeIngredients: UILabel!
    @IBOutlet weak var recipeMethods: UILabel!
    var recipe: Recipes?
    var combined = [String]()
    var watchSession:WCSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setview()
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        sendDataToWatch(message: ["reset":true])
    }
    func setview(){
        imageRecipe.sd_setImage(with: URL(string: recipe!.imageURL), completed: nil)
        recipeName.text = recipe?.name
        recipeTimer.text = "\(convertToMinute(sec: recipe!.timers)) Minutes"
        recipeIngredients.text = CombineIngredients(ingredients: recipe!.ingredients)
        recipeMethods.text = changeToParagraph(texts: recipe!.steps).joined(separator: "\n")
        background.layer.cornerRadius = 70
        background.layer.maskedCorners = [.layerMaxXMinYCorner]
        background.clipsToBounds = true
    }
    func convertToMinute(sec:[Int]) -> String{
        print("kepanggil")
        var mins = sec.reduce(0,+)
        mins = mins/60
        if mins == 0 {
            return "15"
        }
        return String(mins)
    }
    func CombineIngredients(ingredients: [Ingredient]) -> String {
        var bahan :String?
        for ingredient in ingredients {
            bahan = "\(ingredient.quantity) \(ingredient.name)"
            combined.append(bahan!)
        }
        combined = changeToParagraph(texts: combined)
        let final = combined.joined(separator: "\n")
        return final
    }
    func changeToParagraph(texts:[String])->[String]{
        var listTexts = [String]()
       let style = NSMutableParagraphStyle()
        style.alignment = .left
        for text in texts{
            let newText = NSMutableAttributedString(string: "\u{2022} \(text)", attributes: [NSAttributedString.Key.paragraphStyle: style])
            listTexts.append(newText.string)
        }
        return listTexts
        
    }
    @IBAction func cookNow(_ sender: Any) {
        let recipeDictionary = ["ingredient": combined,"step": recipe?.steps as Any, "timers": recipe?.timers as Any, "stepStatus": 1] as [String : Any]
        sendDataToWatch(message: recipeDictionary)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DetailPageViewController: WCSessionDelegate{
  
    func sendDataToWatch(message:[String:Any]){
        if let validSession = watchSession, validSession.isReachable {
            validSession.sendMessage(message, replyHandler: nil, errorHandler: { (err) in
                       debugPrint(err)
                   })
               }
    }
    func configureWatchKitSession(){
        if WCSession.isSupported(){
            watchSession = WCSession.default
            watchSession?.delegate = self
            watchSession?.activate()
        }
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        debugPrint(error as Any)
        print("activationDidComplete", activationState)
        print("WCSession.default activated on iPhone")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("sessionbecomeinactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("session deactivate")
    }
 
}
