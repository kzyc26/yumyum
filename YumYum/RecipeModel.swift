//
//  RecipeModel.swift
//  CookIt
//
//  Created by Kezia Yovita Chandra on 21/07/21.
//

import Foundation
struct Recipes: Codable {
    let name: String
    let ingredients: [Ingredient]
    let steps: [String]
    let timers: [Int]
    let imageURL: String
}
// MARK: - Ingredient
struct Ingredient: Codable {
    let quantity, name: String
}


