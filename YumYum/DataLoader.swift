//
//  DataLoader.swift
//  CookIt
//
//  Created by Kezia Yovita Chandra on 21/07/21.
//

import Foundation
public class DataLoader {
    @Published var allRecipes = [Recipes]()
    init() {
        loadJson()
    }
    func loadJson(){
        if let fileLocation = Bundle.main.url(forResource: "Recipes", withExtension: "json"){
            
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([Recipes].self, from: data)
                self.allRecipes = dataFromJson
            }
            catch {
                print(error)
            }
        }
        
    }
}
