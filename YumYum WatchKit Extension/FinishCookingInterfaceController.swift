//
//  FinishCookingInterfaceController.swift
//  YumYum WatchKit Extension
//
//  Created by Kezia Yovita Chandra on 24/08/21.
//

import UIKit
import WatchKit

class FinishCookingInterfaceController: WKInterfaceController {
    @IBAction func finishCooking() {
        WKInterfaceController.reloadRootControllers(withNames: ["FirstPage"], contexts: [])
    }
}
