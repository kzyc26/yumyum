//
//  TimerInterfaceController.swift
//  YumYum WatchKit Extension
//
//  Created by Kezia Yovita Chandra on 23/08/21.
//

import UIKit
import WatchKit

class TimerInterfaceController: WKInterfaceController {
    @IBOutlet weak var startButton: WKInterfaceButton!
    @IBOutlet weak var watchTimer: WKInterfaceTimer!
    @IBOutlet weak var skipButton: WKInterfaceButton!
    var recipe : [String:Any]?
    var timer : Double = 0.0
    var timerProgress: Double = 0.0
    var systemTimer = Timer()
    var skipStats : buttonText = .skip
    var startStats : buttonText = .start
    var step : Int?
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let newrecipe = context as? [String:Any]{
           recipe = newrecipe
            step = newrecipe["stepStatus"] as? Int
            let recipetimer = recipe!["timers"] as! [Int]
            timer = Double(recipetimer[step! - 2])
            timerProgress = timer
            setTimer(time: timer)
            
            
        } else{
            print("data gak masuk")
        }
    }
    @IBAction func startTimer() {
        switch startStats {
        case .start:
        if timerProgress >= 0.0{
                start()
            }
            
        case .pause:
            if timerProgress > 0.0{
                pause()
            }
         
        case .continueTimer:
            if timerProgress > 0.0{
               continueTimer()
            }
        case .reset:
            start()
            timerProgress = timer
        default:
            break
        }
        
    }
    @IBAction func skipTimer() {
        self.pushController(withName: "InstructionPage", context: recipe)
        systemTimer.invalidate()
    }
    func setTimer(time: Double){
        watchTimer.stop()
        let time = NSDate(timeIntervalSinceNow: TimeInterval(time+1.0))
        watchTimer.setDate(time as Date)
        }
    func start(){
        systemTimer.invalidate()
        setTimer(time: timer)
        watchTimer.start()
        startButton.setTitle(buttonText.pause.rawValue)
        startStats = .pause
        self.systemTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self,selector: #selector(self.timerObserver),userInfo: nil , repeats:  true)
    }
    @objc func timerObserver(){
        self.timerProgress -= 1.0
        print(self.timerProgress)
        if self.timerProgress < 1.0{
            self.systemTimer.invalidate()
            WKInterfaceDevice.current().play(.failure)
            self.startButton.setTitle(buttonText.reset.rawValue)
            self.skipButton.setTitle(buttonText.next.rawValue)
            self.skipStats = .next
            self.startStats = .reset
        }
    }
    func pause(){
        watchTimer.stop()
        systemTimer.invalidate()
        startButton.setTitle(buttonText.continueTimer.rawValue)
        startStats = .continueTimer
        
    }
    func continueTimer(){
        setTimer(time: timerProgress)
        watchTimer.start()
        startButton.setTitle(buttonText.pause.rawValue)
        startStats = .pause
        self.systemTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self,selector: #selector(self.timerObserver),userInfo: nil , repeats:  true)
    }
    
    enum buttonText: String {
        case start = "Start"
        case next = "Next"
        case reset = "Reset"
        case skip = "Skip"
        case pause = "Pause"
        case continueTimer = "Continue"
    }
    }

