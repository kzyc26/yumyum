//
//  IngredientRowController.swift
//  YumYum WatchKit Extension
//
//  Created by Kezia Yovita Chandra on 20/08/21.
//

import UIKit
import WatchKit

class IngredientRowController: NSObject {
    @IBOutlet weak var ingredientName: WKInterfaceLabel!
    @IBOutlet weak var rowGroup: WKInterfaceGroup!
}
