//
//  IngredientsInterfaceController.swift
//  YumYum WatchKit Extension
//
//  Created by Kezia Yovita Chandra on 19/08/21.
//

import UIKit
import WatchConnectivity
import WatchKit
class IngredientsInterfaceController: WKInterfaceController {

    
    @IBOutlet weak var ingredientTable: WKInterfaceTable!
    var status = [Bool]()
    var recipe : [String:Any]?
    var ingredients = [String]()
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let newrecipe = context as? [String:Any]{
            recipe = newrecipe
            ingredients = recipe!["ingredient"] as! [String]
        } else{
            print("data gak masuk")
        }
        configureTableView()
    }
    func configureTableView(){
        ingredientTable.setNumberOfRows(ingredients.count, withRowType: "IngredientRow")
        for ingredient in ingredients {
            let row = ingredientTable.rowController(at: ingredients.firstIndex(of: ingredient)!) as? IngredientRowController
            status.append(false)
            row?.ingredientName.setText(ingredient)
        }
    }
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let row = ingredientTable.rowController(at: rowIndex) as? IngredientRowController
        if status[rowIndex] == false{
            row?.rowGroup.setBackgroundColor(#colorLiteral(red: 0.6881476045, green: 0.5584091544, blue: 0.5516262054, alpha: 1))
            status[rowIndex] = true
        }
        else{
            row?.rowGroup.setBackgroundColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1))
            status[rowIndex] = false
        }
    }
    
    @IBAction func startInstruction() {
        self.pushController(withName: "InstructionPage", context: recipe)
    }
   
}
