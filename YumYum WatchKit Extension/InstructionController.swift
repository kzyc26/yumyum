//
//  InstructionController.swift
//  YumYum WatchKit Extension
//
//  Created by Kezia Yovita Chandra on 20/08/21.
//

import UIKit
import WatchKit

class InstructionController: WKInterfaceController {
    
    var recipe : [String:Any]?
    var instruction = [String]()
    var timer = [Int]()
    var step = 1
    var timerStats = false
    @IBOutlet weak var nextButton: WKInterfaceButton!
    @IBOutlet weak var stepLabel: WKInterfaceLabel!
    @IBOutlet weak var instructionLabel: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let newrecipe = context as? [String:Any]{
           recipe = newrecipe
            instruction = (recipe!["step"] as? [String])!
            timer = (recipe!["timers"] as? [Int])!
            step = (recipe!["stepStatus"] as? Int)!
        } else{
            print("data gak masuk")
        }
        setView()
    }
    func setView(){
        if timer[step-1] > 0{
            stepLabel.setText("Step \(step)")
            instructionLabel.setText(instruction[step-1])
            nextButton.setTitle(buttonText.timer.rawValue)
            timerStats = true
        }
        else{
            stepLabel.setText("Step \(step)")
            instructionLabel.setText(instruction[step-1])
            nextButton.setTitle(buttonText.next.rawValue)
            timerStats = false
        }
        
    }
    @IBAction func nextInstruction() {
        step = step + 1
        if step <= timer.count{
            if timerStats == true{
                recipe?.updateValue(step, forKey: "stepStatus")
                self.pushController(withName: "InterfaceTimer", context: recipe)
            }
            else{
                setView()
            }
        } else{
            self.pushController(withName: "CookFinish", context: nil)
        }
        
        
    }
    enum buttonText : String {
        case next = "Next"
        case timer = "Start Timer"
        
    }
}
