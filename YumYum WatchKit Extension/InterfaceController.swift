//
//  InterfaceController.swift
//  YumYum WatchKit Extension
//
//  Created by Kezia Yovita Chandra on 18/08/21.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var recipe : [String: Any] = [:]
    let watchSession = WCSession.default
    @IBOutlet weak var imageView : WKInterfaceImage!
    @IBOutlet weak var startButton: WKInterfaceButton!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
        if WCSession.isSupported(){
        watchSession.delegate = self
        watchSession.activate()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        debugPrint(error as Any)
        print("activationDidComplete", activationState)
        print("WCSession.default activated on iPhone")
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async { [self] in
            if message.count > 1{
                self.recipe = message
                imageView.setImageNamed("Pan@2x")
                startButton.setTitle("Start Cooking")
            } else{
                recipe.removeAll()
                imageView.setImageNamed("Whisk@2x")
                startButton.setTitle("Pick A Recipe")
            }
         
            
        }
    }
    @IBAction func startCooking() {
        if recipe.count > 0{
            self.pushController(withName: "ingredientsList", context: recipe)
        }
       
    }
}
